const path = require('path');
const _ = (...subdir) => path.join(__dirname, 'src', ...subdir);

module.exports = {
	resolve: {
		alias: {
			"@common/*": _("common/*"),
			"@infl0w.backend/*": _('infl0w-backend'),
			"@infl0w.dom/*": _('infl0w-dom'),
			'@infl0w.app.components/*': _('app', 'infl0wdit0r', 'components'),
			'@infl0w.app.controls/*': _('app', 'infl0wdit0r', 'controls'),
			'@infl0w.app.sockets/*': _('app', 'infl0wdit0r', 'sockets'),
		}
	}
};
