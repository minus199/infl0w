const JsRenderPlugin = {
	install(editor, params = {}) {
		editor.on('rendercontrol', ({ el, control }) => {
			if (control.render && control.render !== 'js') { return; }

			control.handler(el, editor);
		});
	}
};
