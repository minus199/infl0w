import { Component, AfterViewInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import * as ConnectionPlugin from 'rete-alight-render-plugin';
import * as AlightRenderPlugin from 'rete-connection-plugin';
import AddComponent from '@infl0w.app.components/add-component';
import NumComponent from '@infl0w.app.components/number-component';
import NodeEditor from '@infl0w.dom/node-editor';
import Engine from '@infl0w.backend/engine/engine';
const editorIdProvider = () => `{'infl0w'}@{'v0.0.1}`;

@Component({
	selector: 'app-infl0w',
	template: '<div class="wrapper"><div #nodeEditor class="node-editor"></div></div>',
	styleUrls: ['./main-container.component.css'],
	encapsulation: ViewEncapsulation.None
})
export class InFl0wMainContainerComponent implements AfterViewInit {
	@ViewChild('nodeEditor')
	el: ElementRef;
	private editor = null;

	async ngAfterViewInit() {
		const container = this.el.nativeElement;

		const editor = new NodeEditor(editorIdProvider(), container);
		editor.use(ConnectionPlugin, { curvature: 0.4 });
		editor.use(AlightRenderPlugin);

		const engine = new Engine(editorIdProvider());
		const components = [NumComponent, AddComponent].map((cKlz) => new cKlz().register(editor, engine));
		const nodes = Promise.all();
		const n1 = await components[0].createNode({ num: 2 });
		n1.position = [80, 200];
		editor.addNode(n1);
		const n2 = await components[0].createNode({ num: 0 });
		n2.position = [80, 400];
		editor.addNode(n2);
		const add = await components[1].createNode();
		add.position = [500, 240];
		editor.addNode(add);

		editor.connect(
			n1.outputs.get('NumberOut1'),
			add.inputs.get('NumberIn1')
		);

		editor.connect(
			n2.outputs.get('NumberOut1'),
			add.inputs.get('NumberIn2')
		);

		editor.on('process nodecreated noderemoved connectioncreated connectionremoved', async () => {
			await engine.abort();
			await engine.process(editor.toJSON());
		});

		editor._view.resize();
		editor.trigger('process');
	}
}
