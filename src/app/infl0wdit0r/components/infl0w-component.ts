import Node from '@infl0w.backend/node';
import NodeEditor from '@infl0w.dom/node-editor';
import Engine from '@infl0w.backend/engine/engine';
import Component from '@infl0w.backend/component';

export abstract class Payload<K, V> {
	constructor() {
		this._store = new Map<K, V>();
	}
	private _store: Map<K, V>;

	set(k: K, v: V): Payload<K, V> {
		if (this._store.has(k)) {
			throw new Error('Runtime - key already exists. Use update()');
		}

		this._store.set(k, v);
		return this;
	}

	update(k: K, v: V): Payload<K, V> {
		this._store.set(k, v);
		return this;
	}

	get(k: K): V {
		throw new Error('Method not implemented.');
	}
}

/**
 * @param P type of payload
 * @parm K - K from payload for typing
 * @parm V - V from payload for typing
 */
export abstract class InFl0wComponent<K, V, P extends Payload<K, V>> extends Component {
	constructor(name: string) {
		super(name);
	}

	public register(editor: NodeEditor, engine: Engine): InFl0wComponent<K, V, P> {
		engine.register(this);
		editor.register(this);
		return this;
	}

	genName(name: string, component: Component): string {
		return `${name}-${component.name}`;
	}

	onCreated(node: Node) {}

	onDestroyed(node: Node) {}
}
