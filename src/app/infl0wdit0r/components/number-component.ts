import Output from '@infl0w.backend/core/io/output';
import { numSocket } from '@infl0w.app.sockets/sockets';
import { NumControl } from '@infl0w.app.controls/number-control';
import { InFl0wComponent, Payload } from '@infl0w.app.components/infl0w-component';

export default class NumComponent extends InFl0wComponent<string, number, NumberPayload> {
	constructor() {
		super('Number');
	}

	builder(node) {
		const out1 = new Output('NumberOut1', 'title for out1 control', numSocket);
		return node.addControl(new NumControl(this._editor, 'num')).addOutput(out1);
	}

	worker(node, inputs, outputs) {
		outputs[0] = node.data.num;
	}
}

class NumberPayload extends Payload<string, number> {}
