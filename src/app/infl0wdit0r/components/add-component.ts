import { numSocket } from '@infl0w.app.sockets/sockets';
import { NumControl } from '@infl0w.app.controls/number-control';
import { InFl0wComponent, Payload } from '@infl0w.app.components/infl0w-component';
import Input from '@infl0w.backend/core/io/input';
import Output from '@infl0w.backend/core/io/output';
import Node from '@infl0w.backend/node';

class AddComponentPayload extends Payload<string, number> {}

export default class AddComponent extends InFl0wComponent<String, Number, AddComponentPayload> {
	constructor() {
		super('Add');
	}

	async builder(node: Node) {
		const inp1 = new Input('NumberIn1', 'title for NumberInp1', numSocket);
		inp1.addControl(new NumControl(this._editor, 'numCtl1'));
		const inp2 = new Input('NumberIn2', 'title for NumberInp2', numSocket);
		inp2.addControl(new NumControl(this._editor, 'numCtl2'));
		const out = new Output('NumberOut1', 'title for NumberOut', numSocket);

		node.addInput(inp1)
			.addInput(inp2)
			.addControl(new NumControl(this._editor, null, true))
			.addOutput(out);
	}

	worker(node, inputs, outputs) {
		const n1 = inputs[0].length ? inputs[0][0] : node.data.num1;
		const n2 = inputs[1].length ? inputs[1][0] : node.data.num2;
		const sum = n1 + n2;

		const ctrl = <NumControl>this._editor._nodes.find(n => n.id === node.id).controls[0];
		ctrl.setValue(sum);
		outputs[0] = sum;
	}

	onCreated(node) {
		console.log('created', node);
	}

	onDestroyed(node) {
		console.log('destroyed', node);
	}
}
