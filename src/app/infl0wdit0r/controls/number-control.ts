import Control from '@infl0w.backend/control';

export class NumControl extends Control {
	template = '<input type="number" :readonly="readonly" :value="value" @input="change($event)"/>';
	scope = {
		value: 0,
		readonly: false,
		change: this.change.bind(this)
	};
	_alight: any;

	constructor(public emitter, public key, readonly = false) {
		super(key, null, null);
		this.scope.readonly = readonly;
	}

	change(e) {
		this.scope.value = +e.target.value;
		this.update();
	}

	update() {
		if (this._key) {
			this.putData(this._key, this.scope.value);
		}

		this.emitter.trigger('process');
		this._alight.scan();
	}

	mounted() {
		this.scope.value = this.getData(this._key) || 0;
		this.update();
	}

	setValue(val) {
		this.scope.value = val;
		this._alight.scan();
	}
}
