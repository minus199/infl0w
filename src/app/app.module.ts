import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { InFl0wMainContainerComponent } from './infl0wdit0r/main-container.component';

@NgModule({
	declarations: [AppComponent, InFl0wMainContainerComponent],
	imports: [BrowserModule],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
