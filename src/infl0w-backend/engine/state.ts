export namespace Statefullness {
	// const State = { AVALIABLE: 0, PROCESSED: 1, ABORT: 2 };
	export class ConstrainedState {
		constructor(applied: State, constraintsContinue: State[], contraintsBlock: State[]) {}
	}
	export const enum State {
		AVALIABLE = 1 << 1,
		PROCESSING = 1 << 2,
		PROCESSED = 1 << 4,
		FAILED = 1 << 8,
		ABORT = 1 << 16
	}

	export interface IStateful {
		getCurrentState(): State;
	}

	export function a(statefull: IStateful, nextState: State): boolean {
		const mapping = {
			[State.AVALIABLE]: [State.PROCESSING],
			[State.PROCESSING]: [State.PROCESSED, State.FAILED, State.ABORT],
			[State.PROCESSED]: [State.FAILED, State.ABORT],
			[State.FAILED]: [State.ABORT],
			[State.ABORT]: [State.AVALIABLE]
		};

		return nextState in mapping[statefull.getCurrentState()];
	}
}
