import WorkerComponent from '@infl0w.backend/engine/worker-component';
import Context from '@infl0w.backend/core/context';
import EngineEvents from '@infl0w.backend/engine/events';

import Node from '@infl0w.backend/node';
import Input from '@infl0w.backend/core/io/input';
import Emitter from '@common/emitter';
import { ProcessValidation } from '@infl0w.backend/engine/validation';
import {Statefullness} from '@infl0w.backend/engine/state';
// import validate = ProcessValidation.validate; // from '@infl0w.backend/engine/validation';

export default class Process extends Emitter implements Statefullness.IStateful {

	private _components: Array<WorkerComponent>;
	private _args: any[];
	private _data: {};
	private _state: Statefullness.State;
	_id: string;

	constructor(id: string) {
		super(new EngineEvents());
		this._id = id;
		this._components = [];
		this._args = [];
		this._data = null;
		this._state = Statefullness.State.AVALIABLE;
	}

	start(data: Object): boolean {
		if (!ProcessValidation.validate(this.id, data)) {
			return false;
		}

		if (this.state === Statefullness.State.AVALIABLE) {
			this._state = Statefullness.State.PROCESSED;
			return true;
		}

		if (this.state === Statefullness.State.ABORT) {
			return false;
		}

		throw new Error(
			`Runtime - The process is busy and has not been restarted. Use abort() to force it to complete`
		);
	}

	async process(data: {}, startId: number = null, ...args) {
		if (!this.start(data)) {
			return;
		}

		this._data = this.copy(data);
		this._args = args;

		await this.processStartNode(startId);
		await this.processUnreachable();

		return this.done() ? 'success' : 'aborted';
	}

	async forwardProcess(node: Node) {
		if (this.state === State.ABORT) {
			return null;
		}

		return await Promise.all(
			Object.keys(node.outputs).map(async key => {
				const output = node.outputs[key];

				return await Promise.all(
					output.connections.map(async c => {
						const nextNode = this.data.nodes[c.node];

						await this.processNode(nextNode);
						await this.forwardProcess(nextNode);
					})
				);
			})
		);
	}

	done() {
		const success = this.state !== Statefullness.State.ABORT;

		this._state = Statefullness.State.AVALIABLE;

		if (!success) {
			this.abort();
		}

		return success;
	}

	async abort() {
		return new Promise(ret => {
			if (this.state === Statefullness.State.PROCESSED) {
				this._state = Statefullness.State.ABORT;
				this.onAbort = ret;
			} else if (this.state === Statefullness.State.ABORT) {
				this.onAbort();
				this.onAbort = ret;
			} else {
				ret();
			}
		});
	}

	async extractInputData(node: Node): Promise<Array<Input>> {
		const obj = {};

		const y = node.inputs.get('foo').connections[0];

		for (const [key, val] of node.inputs) {
			const promisedOutputs = node.inputs[key].connections.map(async c => {
				const prevNode = this.data.nodes[c.node];

				const outputs = await this.processNode(prevNode);

				if (outputs) {
					return outputs[c.output];
				}

				this.abort();
			});

			obj[key] = await Promise.all(promisedOutputs);
		}

		return obj;
	}

	async processWorker(node: Node) {
		const component = this.components.find(c => c.name === node.name);
		const inputData = await this.extractInputData(node);

		const outputData = {};

		try {
			await component.worker(node, inputData, outputData, ...this.args);
		} catch (e) {
			this.abort();
			this.trigger('warn', e);
		}

		return outputData;
	}

	async processNode(node: Node) {
		if (this.state === State.ABORT || !node) {
			return null;
		}

		await node.lock();

		node.outputData = node.outputData || this.processWorker(node);

		node.unlock();
		return node.outputData;
	}

	copy(data) {
		data = Object.assign({}, data);
		data.nodes = Object.assign({}, data.nodes);

		Object.keys(data.nodes).forEach(key => {
			data.nodes[key] = Object.assign({}, data.nodes[key]);
		});
		return data;
	}

	async processStartNode(id) {
		if (id) {
			const startNode = this.data.nodes[id];

			if (!startNode) {
				return await this.throwError('Node with such id not found');
			}

			await this.processNode(startNode);
			await this.forwardProcess(startNode);
		}
	}

	async processUnreachable() {
		for (const i in this.data.nodes) {
			// process nodes that have not been reached
			if (typeof this.data.nodes[i].outputData === 'undefined') {
				const node = this.data.nodes[i];

				await this.processNode(node);
				await this.forwardProcess(node);
			}
		}
	}

	public onAbort() {
		return;
	}

	public get id() {
		return this._id;
	}

	public getCurrentState(): Statefullness.State {
		return this._state;
	}

	public get components(): WorkerComponent[] {
		return this._components;
	}
}
