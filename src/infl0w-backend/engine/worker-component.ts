import Engine from '@infl0w.backend/engine/engine';
import Input from '@infl0w.backend/core/io/input';
import Output from '@infl0w.backend/core/io/output';

export default abstract class WorkerComponent {
	private _name: string;
	private _data: {};
	private _engine: Engine;

	constructor(name) {
		this._name = name;
		this._data = {};
		this._engine = null;
	}

	worker(node: Node, inputs: Array<Input>, outputs: Array<Output>, ...args: any[]) {}

	public get name() {
		return this._name;
	}

	public get data(): {} {
		return this._data;
	}

	public get engine(): Engine {
		return this._engine;
	}
}
