import WorkerComponent from '@infl0w.backend/engine/worker-component';
import Context from '@infl0w.backend/core/context';
import EngineEvents from '@infl0w.backend/engine/events';
import Validator from '@infl0w.backend/core/validator';
import Node from '@infl0w.backend/node';
import Input from '@infl0w.backend/core/io/input';

const State = { AVALIABLE: 0, PROCESSED: 1, ABORT: 2 };

export { WorkerComponent };

export default class Engine extends Context {
	components: Array<WorkerComponent>;
	args: any[];
	data: any;
	state: number;
	onAbort: () => void;

	constructor(id: string) {
		super(id, new EngineEvents());

		this.components = [];
		this.args = [];
		this.data = null;
		this.state = State.AVALIABLE;
		this.onAbort = () => {};
	}

	clone() {
		const engine = new Engine(this._id);

		this.components.map(c => engine.register(c));

		return engine;
	}

	register(component: WorkerComponent) {
		this.components.push(component);
		this.trigger('componentregister', component);
	}

	async throwError(message, data = null) {
		await this.abort();
		this.trigger('error', { message, data });
		this.processDone();

		return 'error';
	}

	extractInputNodes(node, nodes) {
		return Object.keys(node.inputs).reduce((a, key) => {
			return [...a, ...(node.inputs[key].connections || []).reduce((b, c) => [...b, nodes[c.node]], [])];
		}, []);
	}

	detectRecursions(nodes) {
		const nodesArr = Object.keys(nodes).map(id => nodes[id]);
		const findSelf = (node, inputNodes) => {
			if (inputNodes.some(n => n === node)) {
				return node;
			}

			for (let i = 0; i < inputNodes.length; i++) {
				if (findSelf(node, this.extractInputNodes(inputNodes[i], nodes))) {
					return node;
				}
			}

			return null;
		};

		return nodesArr
			.map(node => {
				return findSelf(node, this.extractInputNodes(node, nodes));
			})
			.filter(r => r !== null);
	}

	processStart() {
		if (this.state === State.AVALIABLE) {
			this.state = State.PROCESSED;
			return true;
		}

		if (this.state === State.ABORT) {
			return false;
		}

		console.warn(`The process is busy and has not been restarted. Use abort() to force it to complete`);
		return false;
	}

	processDone() {
		const success = this.state !== State.ABORT;

		this.state = State.AVALIABLE;

		if (!success) {
			this.onAbort();
			this.onAbort = () => {};
		}

		return success;
	}

	async abort() {
		return new Promise(ret => {
			if (this.state === State.PROCESSED) {
				this.state = State.ABORT;
				this.onAbort = ret;
			} else if (this.state === State.ABORT) {
				this.onAbort();
				this.onAbort = ret;
			} else {
				ret();
			}
		});
	}

	unlock(node: Node) {
		node.unlockPool.forEach(a => a());
		node.unlockPool = [];
		node.busy = false;
	}

	async extractInputData(node: Node): Promise<Array<Input>> {
		const obj = {};

		for (const [key, val] of node.inputs) {
			const promisedOutputs = node.inputs[key].connections.map(async c => {
				const prevNode = this.data.nodes[c.node];

				const outputs = await this.processNode(prevNode);

				if (outputs) {
					return outputs[c.output];
				}

				this.abort();
			});

			obj[key] = await Promise.all(promisedOutputs);
		}

		return obj;
	}

	async processWorker(node: Node) {
		const component = this.components.find(c => c.name === node.name);
		const inputData = await this.extractInputData(node);

		const outputData = {};

		try {
			await component.worker(node, inputData, outputData, ...this.args);
		} catch (e) {
			this.abort();
			this.trigger('warn', e);
		}

		return outputData;
	}

	async processNode(node: Node) {
		if (this.state === State.ABORT || !node) {
			return null;
		}

		await node.lock();

		node.outputData = node.outputData || this.processWorker(node);

		node.unlock();
		return node.outputData;
	}

	async forwardProcess(node: Node) {
		if (this.state === State.ABORT) {
			return null;
		}

		return await Promise.all(
			Object.keys(node.outputs).map(async key => {
				const output = node.outputs[key];

				return await Promise.all(
					output.connections.map(async c => {
						const nextNode = this.data.nodes[c.node];

						await this.processNode(nextNode);
						await this.forwardProcess(nextNode);
					})
				);
			})
		);
	}

	copy(data) {
		data = Object.assign({}, data);
		data.nodes = Object.assign({}, data.nodes);

		Object.keys(data.nodes).forEach(key => {
			data.nodes[key] = Object.assign({}, data.nodes[key]);
		});
		return data;
	}

	async validate(data) {
		const checking = Validator.validate(this._id, data);

		if (!checking.success) {
			return await this.throwError(checking.msg);
		}

		const recurentNodes = this.detectRecursions(data.nodes);

		if (recurentNodes.length > 0) {
			return await this.throwError('Recursion detected', recurentNodes);
		}

		return true;
	}

	async processStartNode(id) {
		if (id) {
			const startNode = this.data.nodes[id];

			if (!startNode) {
				return await this.throwError('Node with such id not found');
			}

			await this.processNode(startNode);
			await this.forwardProcess(startNode);
		}
	}

	async processUnreachable() {
		for (const i in this.data.nodes) {
			// process nodes that have not been reached
			if (typeof this.data.nodes[i].outputData === 'undefined') {
				const node = this.data.nodes[i];

				await this.processNode(node);
				await this.forwardProcess(node);
			}
		}
	}

	async process(data: Object, startId: number = null, ...args) {
		if (!this.processStart()) {
			return;
		}
		if (!this.validate(data)) {
			return;
		}

		this.data = this.copy(data);
		this.args = args;

		await this.processStartNode(startId);
		await this.processUnreachable();

		return this.processDone() ? 'success' : 'aborted';
	}
}
