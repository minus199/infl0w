import Process from '@infl0w.backend/engine/proces';

export namespace ProcessValidation {
	export function isValidData(data) {
		return (
			typeof data.id === 'string' &&
			this.isValidId(data.id) &&
			data.nodes instanceof Object &&
			!(data.nodes instanceof Array)
		);
	}

	export function isValidId(id) {
		return /^[\w-]{3,}@[0-9]+\.[0-9]+\.[0-9]+$/.test(id);
	}

	export function validate(id, data) {
		const msg = [];

		if (!this.isValidData(data)) {
			msg.push('Data is not suitable. ');
		}
		if (id !== data.id) {
			msg.push('IDs not equal. ');
		}

		const id1 = id.split('@');
		const id2 = data.id.split('@');

		if (id1[0] !== id2[0]) {
			msg.push('Names don\'t match. ');
		}

		if (id1[1] !== id2[1]) {
			msg.push('Versions don\'t match');
		}

		return { success: msg.length === 0, msg };
	}

	export async function throwError(process: Process, message, data = null) {
		await process.abort();
		process.trigger('error', { message, data });
		process.done();

		return 'error';
	}

	export async function validateEngine(data) {
		const checking = ProcessValidation.validate(this._id, data);

		if (!checking.success) {
			return await this.throwError(checking.msg);
		}

		const recurentNodes = this.detectRecursions(data.nodes);

		if (recurentNodes.length > 0) {
			return await this.throwError('Recursion detected', recurentNodes);
		}

		return true;
	}

	export function detectRecursions(nodes) {
		const nodesArr = Object.keys(nodes).map(id => nodes[id]);
		const findSelf = (node, inputNodes) => {
			if (inputNodes.some(n => n === node)) {
				return node;
			}

			for (let i = 0; i < inputNodes.length; i++) {
				if (findSelf(node, inputNodes[i].extractInputNodes(nodes))) {
					return node;
				}
			}

			return null;
		};

		return nodesArr.map(node => findSelf(node, node.extractInputNodes(node, nodes))).filter(r => r !== null);
	}
}
