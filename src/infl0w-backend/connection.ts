import Output from '@infl0w.backend/core/io/output';
import Input from '@infl0w.backend/core/io/input';
export default class Connection {
	private _data: {};
	private _output: Output;
	private _input: Input;

	constructor(output, input) {
		this._output = output;
		this._input = input;
		this._data = {};

		this._input.addConnection(this);
	}

	disconnect(): void {
		this._input.removeConnection(this);
		this._output.removeConnection(this);
	}

	isConnected(): Boolean {
		return this._input.hasConnection(this) && this._output.hasConnection(this);
	}

	public get input(): Input {
		return this._input;
	}

	public get output(): Output {
		return this._output;
	}

	public get data(): {} {
		return this._data;
	}
}
