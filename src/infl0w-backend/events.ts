import Events from './core/events';
import { TypedEvent } from '@common/per-event-emitter';

// foo() {
// 	const onFoo = new TypedEvent<String>();
// 	const onBar = new TypedEvent<Number>();

// 	// Emit:
// 	onFoo.emit('bang');
// 	onBar.emit(5);
// 	// Listen:
// 	onFoo.on((foo) => console.log(foo));
// 	onBar.on((bar) => console.log(bar));
// }

export default class EditorEvents extends Events {
	constructor() {
		super({
			nodecreate: [],
			nodecreated: [],
			noderemove: [],
			noderemoved: [],
			connectioncreate: [],
			connectioncreated: [],
			connectionremove: [],
			connectionremoved: [],
			nodetranslate: [],
			nodetranslated: [],
			selectnode: [],
			nodeselect: [],
			nodeselected: [],
			rendernode: [],
			rendersocket: [],
			rendercontrol: [],
			renderconnection: [],
			updateconnection: [],
			componentregister: [],
			keydown: [],
			keyup: [],
			translate: [],
			translated: [],
			zoom: [],
			zoomed: [],
			click: [],
			mousemove: [],
			contextmenu: [],
			import: [],
			export: [],
			process: []
		});
	}

	handlers: any;
}
