import IO from '@infl0w.backend/core/io/input';

export default class Output extends IO {
	toJSON() {
		return {
			connections: this.connections.map(c => {
				return {
					node: c.input.node.id,
					input: c.input.key,
					output: null,
					data: c.data
				};
			})
		};
	}
}
