import Connection from '@infl0w.backend/connection';
import Socket from '@infl0w.backend/core/io/socket';
import Input from '@infl0w.backend/core/io/input';
import NodeInternal from '@infl0w.backend/node-internal';
import Connectable from '@infl0w.backend/core/contracts/connectable';

export default abstract class Pipe extends NodeInternal implements Connectable {
	multipleConnections: Boolean;
	connections: Connection[];

	name: string;
	socket: Socket;

	constructor(key: string, name: string, socket: Socket, multiConns: Boolean = false) {
		super(Symbol(key), null, null);

		this.multipleConnections = multiConns;
		this.connections = [];
		this.socket = socket;
	}

	addConnection(connection: Connection): Pipe {
		if (!this.multipleConnections && this.hasConnection()) {
			throw new Error('Multiple connections not allowed');
		}

		this.connections.push(connection);
		return this;
	}

	connectTo(io: Pipe, connData = {}): Connection {
		if (!this.socket.compatibleWith(io.socket)) {
			throw new Error('Sockets not compatible');
		}

		if (!io.multipleConnections && io.hasConnection()) {
			throw new Error('Input already has one connection');
		}

		if (!this.multipleConnections && this.hasConnection()) {
			throw new Error('Output already has one connection');
		}

		const connection = new Connection(this, io);
		this.connections.push(connection);
		return connection;
	}

	connectedTo(input: Input): boolean {
		return this.connections.some(item => item.input === input);
	}

	removeConnection(connection: Connection): Pipe {
		this.connections.splice(this.connections.indexOf(connection), 1);
		return this;
	}

	removeConnections(): Pipe {
		this.connections.map(connection => this.removeConnection(connection));
		return this;
	}

	hasConnection(connection: Connection = null) {
		if (connection !== null) {
			return this.connections.some(conn => conn === connection);
		}

		return this.connections.length > 0;
	}
}
