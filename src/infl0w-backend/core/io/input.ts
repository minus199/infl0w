import Connection from '@infl0w.backend/connection';
import Control from '@infl0w.backend/control';
import Pipe from '@infl0w.backend/core/io/pipe';
import Socket from '@infl0w.backend/core/io/socket';

export default class Input extends Pipe {
	constructor(key: string, title: string, socket: Socket, multiConns: boolean = false) {
		super(key, title, socket, multiConns);
		this.control = null;
	}

	control: Control;
	connections: Array<Connection>;
	multipleConnections: Boolean;

	addControl(control: Control): Input {
		this.control = control;
		control.bindParent(this);
		return this;
	}

	showControl() {
		return !this.hasConnection() && this.control !== null;
	}

	toJSON() {
		return {
			connections: this.connections.map(c => {
				return {
					node: c.output._node.id,
					output: c.output._key,
					input: null,
					data: c.data
				};
			})
		};
	}
}
