import Emitter from '@common/emitter';
import Validator from '../engine/validation';

export default class Context extends Emitter {
	_id: any;

	constructor(id, events) {
		super(events);

		if (!Validator.isValidId(id)) {
			throw new Error('ID should be valid to name@0.1.0 format');
		}

		this._id = id;
	}

	use(plugin, options = {}) {
		plugin.install(this, options);
	}
}
