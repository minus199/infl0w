import Connection from '@infl0w.backend/connection';
import Input from '@infl0w.backend/core/io/input';
import Pipe from '@infl0w.backend/core/io/pipe';

export default interface Connectable {
	addConnection(connection: Connection): Pipe;
	connectTo(io: Pipe): Connection;
	connectedTo(input: Input): boolean;
	removeConnection(connection: Connection): Pipe;
	removeConnections(): Pipe;
	hasConnection(connection: Connection): {};
}
