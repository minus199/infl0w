import { Serializer } from '@angular/compiler';

/**
 * @param T The type to be serialized
 * @param R The raw dictionary
 */
export interface ISerializer<T, R> {
	from(rawData: string | R): T;

	populate(T): R;
}

interface RFoo {
	myFoo: string;
	yourFoo: number;
	someoneElsesFoo: never;
}

class Foo {
	readonly _serializer: ISerializer<Foo, RFoo>;

	public get serializer(): ISerializer<Foo, RFoo> {
		return this._serializer;
	}
}
