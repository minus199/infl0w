import Node from '@infl0w.backend/node';

export default abstract class NodeInternal {
	protected _key: string;
	protected _data: {};

	protected _parent: NodeInternal | Node;
	protected _node: Node;

	constructor(key, parent, node, data = {}) {
		this._key = key;
		this._data = data;
		this._parent = parent;
		this._node = node;
	}

	bindNode(node: Node): NodeInternal {
		if (this._node !== null) {
			throw new Error(`AlreadyBoundException - current internal is already bound to node ${this.key}`);
		}

		this._node = node;
		return this;
	}

	unbindNode(): NodeInternal {
		this._node = null;
		return this;
	}

	bindParent(parent): NodeInternal {
		if (this._parent !== null) {
			throw new Error(`AlreadyBoundException - current internal is already bound to node ${this.parent}`);
		}

		this._parent = parent;
		return this;
	}

	public get node(): Node {
		if (this._parent === null) {
			throw new Error('Control isn\'t added to Node/Input');
		}

		return this._parent instanceof Node ? this._parent : this._parent._node;
	}

	public get key(): string {
		return this._key;
	}

	public get parent() {
		return this.parent;
	}

	public get data() {
		return this.node.data;
	}
}
