import Control from '@infl0w.backend/control';
import Input from '@infl0w.backend/core/io/input';
import Output from '@infl0w.backend/core/io/output';
// import {v4} from 'node-uuid';
import { v4 } from 'uuid/interfaces';
import Pipe from '@infl0w.backend/core/io/pipe';
import NodeEditor from '@infl0w.dom/node-editor';

export default class Node {
	private static latestId: any;

	private _name: string;

	private _id: number;

	private _position: number[];

	private _inputs: Map<string, Input>;

	private _outputs: Map<string, Output>;

	private _controls: Map<string, Control>;

	private _data: {};

	private _meta: {};
	constructor(name: string, data = {}, meta = {}) {
		this._name = name;

		this._id = Node.incrementId();
		this._position = [0.0, 0.0];

		this._inputs = new Map();
		this._outputs = new Map();
		this._controls = new Map();

		this._data = data;
		this._meta = meta;
	}

	static incrementId() {
		if (!this.latestId) {
			this.latestId = 1;
		} else {
			this.latestId++;
		}
		return this.latestId;
	}

	static fromJSON(json: string) {
		const node: Node = JSON.parse(json);
		Node.latestId = Math.max(node.id, Node.latestId);
		return node;
	}

	toJSON() {
		return {
			id: this._id,
			data: this._data,
			inputs: Array.from(this._inputs).reduce((obj, [key, input]) => ((obj[key] = input.toJSON()), obj), {}),
			outputs: Array.from(this._outputs).reduce((obj, [key, output]) => ((obj[key] = output.toJSON()), obj), {}),
			position: this._position,
			name: this.name
		};
	}

	addControl(control: Control) {
		control.bindParent(this);
		this._controls.set(control.key, control);
		return this;
	}

	addInput(input: Input) {
		input.bindNode(this);
		this._inputs.set(input.key.toString(), input);
		return this;
	}

	addOutput(output: Output) {
		output.bindNode(this);
		this._outputs.set(output.key, output);
		return this;
	}

	removeControl(control: Control) {
		control.unbindNode();
		this._controls.delete(control.key);
	}

	removeInput(input: Input) {
		input.removeConnections();
		input.unbindNode();

		this._inputs.delete(input.key);
	}

	removeOutput(output: Output) {
		output.removeConnections();
		output.unbindNode();
		this._outputs.delete(output.key);
	}

	disconnect(editor: NodeEditor): boolean {
		const ios: Array<Pipe> = [...this._inputs.values(), ...this._outputs.values()];
		ios.forEach(io => editor.disconnect(...io.connections));

		return true;
	}

	extractInputNodes(nodes): Node[] {
		return Object.keys(this.inputs).reduce((a, key) => {
			return [...a, ...(this.inputs[key].connections || []).reduce((b, c) => [...b, nodes[c.node]], [])];
		}, []);
	}

	async lock() {
		return new Promise(res => {
			node.unlockPool = node.unlockPool || [];
			if (node.busy && !node.outputData) {
				node.unlockPool.push(res);
			} else {
				res();
			}

			node.busy = true;
		});
	}

	unlock() {
		node.unlockPool.forEach(a => a());
		node.unlockPool = [];
		node.busy = false;
	}

	public get name() {
		return this.name;
	}

	public get id() {
		return this._id;
	}

	public get position() {
		return this.position;
	}

	public get inputs() {
		return this._inputs;
	}

	public get outputs() {
		return this._outputs;
	}

	public get controls() {
		return this._controls;
	}

	public get data() {
		return this._data;
	}
}
