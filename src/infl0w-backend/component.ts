import Node from '@infl0w.backend/node';
import NodeEditor from '@infl0w.dom/node-editor';
import { WorkerComponent } from '@infl0w.backend/engine/engine';

export default abstract class Component extends WorkerComponent {
	protected _editor: NodeEditor;

	protected constructor(name) {
		super(name);
		this._editor = null;
	}

	abstract genName(name: string, component: Component): string;

	abstract async builder(node: Node);

	abstract onCreated(node: Node);

	abstract onDestroyed(node: Node);

	async build(node: Node) {
		await this.builder(node);
		return node;
	}

	async createNode(data = {}, meta = {}) {
		const node = new Node(this.name, data, meta);
		await this.build(node);

		return node;
	}

	public bindEditor(editor: NodeEditor): Component {
		if (this._editor) {
			throw new Error('AlreadyBoundException - Editor instance was already bound to this componenet.');
		}

		this._editor = editor;
		return this;
	}
}
