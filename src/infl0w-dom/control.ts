import Emitter from '@common/emitter';

export default class ViewControl extends Emitter {
	constructor(el, control, emitter) {
		super(emitter);
		this.trigger('rendercontrol', { el, control });
	}
}
