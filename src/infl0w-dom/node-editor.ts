import Component from '@infl0w.backend/component';
import Connection from '@infl0w.backend/connection';
import Context from '@infl0w.backend/core/context';
import EditorEvents from '@infl0w.backend/events';
import EditorView from '@infl0w.dom/editor-view';
import Input from '@infl0w.backend/core/io/input';
import Node from '@infl0w.backend/node';
import Output from '@infl0w.backend/core/io/output';
import Selected from '@infl0w.backend/selected';
import Validator from '@infl0w.backend/core/validator';

export default class NodeEditor extends Context {
	_nodes: Node[];
	_components: Map<any, any>;
	_selected: Selected;
	_view: EditorView;
	_id: any;
	_silent: boolean;

	constructor(id: string, container: HTMLElement) {
		super(id, new EditorEvents());

		this._nodes = [];
		this._components = new Map();

		this._selected = new Selected();
		this._view = new EditorView(container, this._components, this);

		window.addEventListener('keydown', e => this.trigger('keydown', e));
		window.addEventListener('keyup', e => this.trigger('keyup', e));
		this.on('nodecreated', (node: Node) => {
			return this.getComponent(node.name).created(node);
		});

		this.on('noderemoved', (node: Node) => this.getComponent(node.name).destroyed(node));
		this.on('selectnode', ({ node, accumulate }) => this.selectNode(node, accumulate));
	}

	addNode(node: Node) {
		if (!this.trigger('nodecreate', node)) {
			return;
		}

		this._nodes.push(node);
		this._view.addNode(node);

		this.trigger('nodecreated', node);
	}

	removeNode(node: Node): boolean {
		if (!this.trigger('noderemove', node)) {
			return false;
		}

		node.disconnect(this);

		this._nodes.splice(this._nodes.indexOf(node), 1);
		this._view.removeNode(node);

		this.trigger('noderemoved', node);

		return true;
	}

	connect(output: Output, input: Input, data = {}) {
		if (!this.trigger('connectioncreate', { output, input })) {
			return;
		}

		try {
			const connection = output.connectTo(input, data);
			this._view.connect(connection);

			this.trigger('connectioncreated', connection);
		} catch (e) {
			this.trigger('warn', e);
		}
	}

	disconnect(...connections: Connection[]): boolean {
		connections.forEach(connection => {
			if (!this.trigger('connectionremove', connection)) {
				return;
			}

			this._view.disconnect(connection);
			connection.disconnect();

			this.trigger('connectionremoved', connection);
		});

		return true;
	}

	selectNode(node: Node, accumulate: boolean = false) {
		if (this._nodes.indexOf(node) === -1) {
			throw new Error('Node not exist in list');
		}

		if (!this.trigger('nodeselect', node)) {
			return;
		}

		this._selected.add(node, accumulate);

		this.trigger('nodeselected', node);
	}

	getComponent(name) {
		const component = this._components.get(name);

		if (!component) {
			throw new Error(`Component ${name} not found`);
		}

		return component;
	}

	register(component: Component) {
		component.bindEditor(this);
		this._components.set(component.name, component);
		this.trigger('componentregister', component);
	}

	clear() {
		[...this._nodes].map(node => this.removeNode(node));
	}

	toJSON() {
		const data = { id: this._id, nodes: {} };

		this._nodes.forEach(node => (data.nodes[node.id] = node.toJSON()));
		this.trigger('export', data);
		return data;
	}

	beforeImport(json: Object) {
		const checking = Validator.validate(this._id, json);

		if (!checking.success) {
			this.trigger('warn', checking.msg);
			return false;
		}

		this._silent = true;
		this.clear();
		this.trigger('import', json);
		return true;
	}

	afterImport() {
		this._silent = false;
		return true;
	}

	async fromJSON(json: { nodes: Array<Node> }) {
		if (!this.beforeImport(json)) {
			return false;
		}
		const nodes = {};

		try {
			await Promise.all(
				Object.keys(json.nodes).map(async id => {
					const node = json.nodes[id];
					const component = this.getComponent(node.name);

					nodes[id] = await component.build(Node.fromJSON(node));
					this.addNode(nodes[id]);
				})
			);

			Object.keys(json.nodes).forEach(id => {
				const jsonNode = json.nodes[id];
				const node = nodes[id];

				Object.keys(jsonNode.outputs).forEach(key => {
					const outputJson = jsonNode.outputs[key];

					outputJson.connections.forEach(jsonConnection => {
						const nodeId = jsonConnection.node;
						const data = jsonConnection.data;
						const targetOutput = node.outputs.get(key);
						const targetInput = nodes[nodeId].inputs.get(jsonConnection.input);

						this.connect(
							targetOutput,
							targetInput,
							data
						);
					});
				});
			});
		} catch (e) {
			this.trigger('warn', e);
			return !this.afterImport();
		} finally {
			return this.afterImport();
		}
	}

	public get nodes(): Node[] {
		return this._nodes;
	}

	public get components(): Map<any, any> {
		return this._components;
	}

	public get selected(): Selected {
		return this._selected;
	}

	public get view(): EditorView {
		return this._view;
	}

	public get id(): any {
		return this._id;
	}

	public get silent(): boolean {
		return this._silent;
	}
}
