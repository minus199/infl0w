import Emitter from '@common/emitter';
import ViewNode from './node';
import Connection from '@infl0w.backend/connection';

export default class ViewConnection extends Emitter {
	connection: Connection;
	inputNode: ViewNode;
	outputNode: ViewNode;
	el: HTMLDivElement;

	constructor(connection, inputNode, outputNode, emitter) {
		super(emitter);
		this.connection = connection;
		this.inputNode = inputNode;
		this.outputNode = outputNode;

		this.el = document.createElement('div');
		this.el.style.position = 'absolute';
		this.el.style.zIndex = '-1';

		this.trigger('renderconnection', {
			el: this.el,
			connection: this.connection,
			points: this.getPoints()
		});
	}

	getPoints() {
		const [x1, y1] = this.outputNode.getSocketPosition(this.connection.output);
		const [x2, y2] = this.inputNode.getSocketPosition(this.connection.input);

		return [x1, y1, x2, y2];
	}

	update() {
		this.trigger('updateconnection', {
			el: this.el,
			connection: this.connection,
			points: this.getPoints()
		});
	}
}
