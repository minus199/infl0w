import NodeEditor from '@infl0w.dom/node-editor';
export default interface AbstractInfl0win {
	install(editor: NodeEditor, params: Map<string, any>);
}
