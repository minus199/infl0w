import Events from '../infl0w-backend/core/events';

export default class Emitter {
	events: any;
	_silent: boolean;
	constructor(events: Events | Emitter) {
		this.events = events instanceof Emitter ? events.events : events.handlers;
		this._silent = false;
	}

	on<U, T>(names: string, handler: (...args: T[]) => U) {
		names.split(' ').forEach(name => {
			if (!this.events[name]) {
				throw new Error(`The event ${name} does not exist`);
			}
			this.events[name].push(handler);
		});

		return this;
	}

	trigger(name: string, ...params) {
		if (!(name in this.events)) {
			throw new Error(`The event ${name} cannot be triggered`);
		}

		return this.events[name].reduce((r, e) => {
			return e(...params) !== false && r;
		}, true); // return false if at least one event is false
	}

	bind(name: string) {
		this.events[name] = [];
	}
}
